if [ ! -d "$HOME/.tmux" ]; then
  mkdir -p "$HOME/.tmux"
fi

rsync -av --exclude=.git --exclude=*.sh --exclude=*.tmux "$PWD/" "$HOME"
rsync -av --include=*.tmux --exclude=* "$PWD/" "$HOME"

# install vim-cpp-modern
if [ ! -d "$HOME/.vim/pack/git-plugins/start" ]; then
  mkdir -p "$HOME/.vim/pack/git-plugins/start"
  git clone --depth=1 https://github.com/bfrg/vim-cpp-modern "$HOME/.vim/pack/git-plugins/start/vim-cpp-modern"
else
  echo "vim-cpp-modern already installed"
fi

if [[ "$OSTYPE" == "darwin"* ]]; then
  echo "You are on Mac..."
elif [[ "$OSTYPE" == "linux-gnu"* ]]; then

  if command -v delta >/dev/null 2>&1; then
    echo "delta already installed"
  else
    echo "installing delta 0.16.5 AMD64"
    wget https://github.com/dandavison/delta/releases/download/0.16.5/git-delta_0.16.5_amd64.deb
    sudo dpkg -i git-delta_0.16.5_amd64.deb
    rm git-delta_0.16.5_amd64.deb
  fi

  vim_ver=`vim --version | head -n 1 | grep -o -E "[0-9]+\.[0-9]"`
  echo "local vim version: $vim_ver"
  vim_ver_main=`echo $vim_ver | grep -o -E "^[0-9]+"`
  if [ $vim_ver_main -lt 9 ]; then
    echo "Installing vim 9"
    sudo add-apt-repository ppa:jonathonf/vim

    sudo apt-get update
    sudo apt-get install build-essential gcc
    sudo apt-get autoremove
  else
    echo "vim 9 already installed"
  fi

fi
