require("config.lazy")

require("tokyonight").setup({
  on_color=function(colors)
    colors.bg = "#000000"
  end,
})

vim.cmd[[colorscheme tokyonight]]

vim.api.nvim_set_keymap('i', 'jj', '<ESC>', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', '<F2>', ':set number!<CR>', { noremap = true, silent = true })
vim.cmd[[
  set termguicolors
  set fenc=utf-8
  set expandtab
  set visualbell
  set tabstop=2
  set shiftwidth=2
  if has('mac')
    set clipboard=unnamed
  else
    set clipboard=unnamedplus
  endif
  set noswapfile
  set ignorecase
  set smartcase
]]

vim.api.nvim_create_autocmd("ExitPre", {
	group = vim.api.nvim_create_augroup("Exit", { clear = true }),
	command = "set guicursor=a:ver90",
	desc = "Set cursor back to beam when leaving Neovim."
})

vim.api.nvim_set_hl(0, "LineNr", { fg = "#FFFFFF", bg = "NONE" })

if vim.loop.os_uname().sysname == "Linux" then
  vim.g.clipboard = {
    name = "xsel",
    copy = {
      ["+"] = "xsel --clipboard --input",
      ["*"] = "xsel --primary --input",
    },
    paste = {
      ["+"] = "xsel --clipboard --output",
      ["*"] = "xsel --primary --output",
    },
    cache_enabled = 0,
  }
end

